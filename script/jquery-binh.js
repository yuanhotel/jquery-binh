/**
 @info:based on jquery.js
 @source:jquery-binh.js
 @author:yuanhotel
 @date:2015-01-05
 设置弹出框的路径及大小
 $.binhData.init({src:'win.html',width:200,height:200});
 集合的格式
 {"oper":0,"left":1,"right":1,"content":{"type":0,"list":[]}}
 **/
(function($){
	$.extend({
		binhData:{
		  oper : ['oper-disable','oper-1','oper-2','oper-3'],
		  operVal : ['','交集','并集','减集'],
		  type : ['type-1','type-2'],
		  typeVal : ['条件与','条件或'],
		  left : ['left-disable','left-1','left-2','left-3'],
		  right : ['right-disable','right-1','right-2','right-3'],
		  curOper : {},
		  defData:{'oper':0,'left':0,'content':{'type':0,'list':[]},'right':0},
		  winstyle:{width:500,height:400,src:'win.html'},
		  init : function(options) {
			$('body').append('<iframe id="binhwin" style="display:none;"></iframe>');
			this.winstyle =$.extend(this.winstyle, options);
		  }
		},
		binhEvent:{
			setoper:function(ele) {
				element = $(ele);
				$.binh.poll($.binhData.oper,element);
				var ind= $.binh.boll($.binhData.oper,element);
				element.text($.binhData.operVal[ind]);
			},
			setleft:function(ele) {
				element = $(ele);
				$.binh.poll($.binhData.left,element);
			},
			setright : function(ele) {
				element = $(ele);
				$.binh.poll($.binhData.right,element);
			},
			settype : function(ele) {
				element = $(ele);
				$.binh.poll($.binhData.type,element);
				var ind= $.binh.boll($.binhData.type,element);
				element.text($.binhData.typeVal[ind]);
			},
			winopen : function(ele) {
				element = $(ele);
				$.binhData.curOper = element.parent().next();
				var top = ($(window).height() - $.binhData.winstyle.height)/2;   
				var left = ($(window).width() - $.binhData.winstyle.width)/2;
				var scrollTop = $(document).scrollTop();   
				var scrollLeft = $(document).scrollLeft();
				$('#binhwin').css({"background":"yellow",
					"position":"absolute",
					"width":$.binhData.winstyle.width,
					"height":$.binhData.winstyle.height,
					"left":left+scrollLeft,
					"top":top+scrollTop});
				$('#binhwin').attr("src",$.binhData.winstyle.src);
				$('#binhwin').show();
			},
			winclose : function() {
				$('#binhwin').hide();
			},
			wincallback : function(data) {
				if($.binhData.curOper && data) {
					var dd = data.split(',');
					for(var i=0;i<dd.length;i++) {
						$.binhData.curOper.append('<li>'+dd[i]+'</li>');
					}
				}
			}
		},
		binh: {
			div2json :function(obj) {
				var jsonstr = '{';
				var operIndex=this.boll($.binhData.oper,obj.find("div.operbar"));
				jsonstr += '"oper":'+operIndex;
				var leftIndex=this.boll($.binhData.left,obj.find("div.leftbar"));
				jsonstr += ',"left":'+leftIndex;
				var rightIndex=this.boll($.binhData.right,obj.find("div.rightbar"));
				jsonstr += ',"right":'+rightIndex;
				var typeIndex=this.boll($.binhData.type,obj.find("span.settype"));
				jsonstr += ',"content":{"type":'+typeIndex+',"list":[';
				var li = new Array();
				obj.find("li").each(function(){
					li.push('"'+$(this).text()+'"');
				});
				jsonstr +=li.toString()+']}}';
				return jsonstr;
			},
			poll :function(arr,obj) {
				for(var i=0;i<arr.length;i++) {
					if(obj.hasClass(arr[i])){
						obj.removeClass(arr[i]);
					   if((i+1)<arr.length) {
							obj.addClass(arr[i+1]);
					   } else {
							obj.addClass(arr[0]);
					   }
					   return;
					}
				}
			},
			boll : function(arr,obj){
				for(var i=0;i<arr.length;i++) {
					if(obj.hasClass(arr[i])){
						return i;
					}
				}
				return -1;
			},
			divdata : function(d){
				var divhtml ='<div class="container">';
				divhtml+='<div class="operbar '+$.binhData.oper[d.oper]+'" onclick="$.binhEvent.setoper(this)"></div>';
				divhtml+='<div class="leftbar '+$.binhData.left[d.left]+'" onclick="$.binhEvent.setleft(this)"></div>';
				divhtml+='<div class="content">';
				divhtml+='<div><span class="settype '+$.binhData.type[d.content.type]+'" onclick="$.binhEvent.settype(this)">'+$.binhData.typeVal[d.content.type]+'</span>';
				divhtml+='<input class="btnwin" type="button" onclick="$.binhEvent.winopen(this)" value="选择标签"/></div>';
				divhtml+='<ul>';
				for(var i=0;i<d.content.list.length;i++) {
					divhtml+='<li>'+d.content.list[i]+'</li>';
				}
				divhtml+='</ul>';
				divhtml+='</div>';
				divhtml+='<div class="rightbar '+$.binhData.right[d.right]+'" onclick="$.binhEvent.setright(this)"></div>';
				divhtml+='<div class="clear"></div>';
				divhtml+='</div>';
				return divhtml;
			}
		}
	});
	$.fn.extend({
		newset: function(data) {
			if(data) {
				var d =$.extend(data,$.binhData.defData);
				var divhtml = $.binh.divdata(d);
				this.append(divhtml);
			}
		},
		tojson: function() {
			var json = new Array();
			this.children().each(function(){
				json.push($.binh.div2json($(this)));
			});
			return '['+json.toString()+']';
		},
		toload : function(data) {
			this.children().remove();
			if(data) {
				for(var i=0;i<data.length;i++) {
					var divhtml = $.binh.divdata(data[i]);
					this.append(divhtml);
				}
			}
		}
	});
})(jQuery);
